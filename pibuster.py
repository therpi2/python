# PiBuster
# This is an example program from Raspberry Pi For Dummies
# See Chapter 12 for a detailed breakdown of how this works
# by Sean McManus and Mike Cook
# Find out more at www.sean.co.uk

import pygame, sys, time, random
from pygame.locals import *

def realx(x):
    x = x * 20
    return x

def realy(y):
    y = y * 20
    return y

def drawbrick(xcoord, ycoord, col):
    if col == 1:
       boxcolor = GREEN
       highlightcolor = LIGHTGREEN
    else:
       boxcolor = RED
       highlightcolor = LIGHTRED
    pygame.draw.rect (gameSurface, boxcolor, (realx(xcoord), realy(ycoord), 20, 20))
    pygame.draw.rect (gameSurface, highlightcolor, (realx(xcoord), realy(ycoord), 2, 20))
    pygame.draw.rect (gameSurface, highlightcolor, (realx(xcoord), realy(ycoord), 20, 2))
    pygame.draw.rect (gameSurface, SHADOW, (realx(xcoord)+18, realy(ycoord), 2, 20))
    pygame.draw.rect (gameSurface, SHADOW, (realx(xcoord), realy(ycoord)+18, 20, 2))

def drawball(x, y):
    pygame.draw.circle(gameSurface, BLUE, (realx(x)+10, realy(y)+10), 10, 0)
    pygame.draw.circle(gameSurface, LIGHTBLUE, (realx(x)+6, realy(y)+6), 2, 0)

def blank(x, y):
    pygame.draw.rect(gameSurface, WHITE, (realx(x), realy(y), 20, 20))

def drawbat(x):
    pygame.draw.rect(gameSurface, LIGHTPURPLE, (realx(x), realy(BATY), 40, 4))
    pygame.draw.rect(gameSurface, PURPLE, (realx(x), realy(BATY)+4, 40, 6))
    pygame.draw.rect(gameSurface, SHADOW, (realx(x), realy(BATY)+10, 40, 2))
    pygame.draw.rect(gameSurface, SHADOW, (realx(x)+38, realy(BATY), 2, 12))
    
def clearbat(x):
    blank(x, BATY)
    blank(x+1, BATY)

def gameover():
    showtext('GAME OVER')
    time.sleep(8)
    endgame()

def endgame():
    pygame.quit()
    sys.exit()

def gamewon():
    showtext('GAME WON!')
    time.sleep(8)
    endgame()

def havetheywon():
    if score == brickcount:
        return True
    else:
        return False
    
def showtext(text):
    fontObj = pygame.font.Font('freesansbold.ttf', 64)
    textsurface = fontObj.render(text, True, PURPLE, WHITE)
    textRectObj = textsurface.get_rect()
    textRectObj.center = (220, 200)
    gameSurface.blit(textsurface, textRectObj)
    pygame.display.update()

pygame.init()

#window
gameSurface = pygame.display.set_mode((440, 480))
pygame.display.set_caption('PiBuster')
pygame.mouse.set_visible(0)

#timings
FPS = 20 # 30 is full speed, 4 for slow motion test
fpsClock = pygame.time.Clock()
pygame.key.set_repeat(1, 1) # If you use 0, 0 it turns the repeat off

#colors
SHADOW = (192, 192, 192)
WHITE = (255, 255, 255)
LIGHTGREEN = (0, 255, 0)
GREEN = (0, 200, 0)
BLUE = (0, 0, 128)
LIGHTBLUE = (0, 0, 255)
RED = (200, 0, 0)
LIGHTRED = (255, 100, 100)
PURPLE = (102, 0, 102)
LIGHTPURPLE = (153, 0, 153)

# draw screen with frame
gameSurface.fill(WHITE)
pygame.draw.rect(gameSurface, PURPLE, (16, 16, 406, 2))
pygame.draw.rect(gameSurface, PURPLE, (16, 16, 2, 440))
pygame.draw.rect(gameSurface, PURPLE, (422, 16, 2, 440))

# set up and show bricks
map=[
#-----0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9--
     [0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,0],
     [0,0,0,1,1,1,1,0,0,0,0,0,0,1,1,1,1,0,0,0],
     [0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1,0,0,0,0],
     [0,0,0,0,0,1,1,1,1,0,0,1,1,1,1,0,0,0,0,0],
     [0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,0,0,0,0,0],
     
     [0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,2,2,2,2,2,2,2,2,0,0,0,0,0,0],
     [0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0],
     [0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0],
     [0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0],
     
     [0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0],
     [0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0],
     [0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0],
     [0,0,0,0,0,0,2,2,2,2,2,2,2,2,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0],

     [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
     ]

brickcount = 0

for x in range(1, 21):
    for y in range (1, 20):
        if map[y-1][x-1] != 0:
            drawbrick(x, y, map[y-1][x-1])
            brickcount += 1


#initialize bat and score
batx = random.randint(1,19)
BATY = 20
drawbat(batx)
score = 0

#initialize ball
ballx = 0
bally = 0
while map[bally-1][ballx-1] != 0 or ballx == 0:
    ballx = random.randint(1, 20)
    bally = random.randint(1, 7)
ballxdir = 1
ballydir = -1
drawball(ballx, bally)

#update screen and short pause
pygame.display.update()
time.sleep(2.5)

#main game loop
while True:
    for event in pygame.event.get():
        oldbat = batx
        if event.type == QUIT:
            endgame()
        if event.type == KEYDOWN:
            if event.key == K_RIGHT:
                batx = batx + 1
            elif event.key == K_LEFT:
                batx = batx -1
        if batx == 0 or batx == 20:
            batx = oldbat
        clearbat(oldbat)
        drawbat(batx)

#move ball
    oldballx = ballx
    oldbally = bally
    if ballx == 1:
        ballxdir = 1           
    if ballx == 20:
        ballxdir = - 1
    if bally == 1:
        ballydir = 1
#Uncomment below for self-playing mode for testing
#    if bally == BATY - 1:
#        ballydir = -1
    if bally == BATY - 1:
        batleft = batx
        batright = batx + 1
        if ballx >= batleft and ballx <= batright:
            ballydir = -1
        if ballx + ballxdir >= batleft and ballx + ballxdir <= batright:
            ballydir = -1
    if bally == BATY:
        gameover()
        
    ballx += ballxdir
    
    if map[bally-1][ballx-1] == 0:
        bally += ballydir

#test for ball hitting a brick and react accordingly
    if map[bally-1][ballx-1] != 0:
        blank(ballx, bally)
        map[bally-1][ballx-1] = 0
        bally = oldbally
        ballx = oldballx
        ballydir = -ballydir
        if random.randint(1, 10) > 5 and map[bally+ballydir-1][ballx-ballxdir-1] == 0:
            ballxdir = -ballxdir
        score += 1     

    blank(oldballx, oldbally)
    drawball(ballx, bally)
    pygame.display.update()
    if havetheywon() == True:
        gamewon()
    fpsClock.tick(FPS)
    


