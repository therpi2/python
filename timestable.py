# simple times table program
# This is an example program from Raspberry Pi For Dummies
# by Sean McManus and Mike Cook
# Find out more at www.sean.co.uk

print "This program calculates times tables"
print "It is from Raspberry Pi For Dummies"

tablenum = input("\nWhich multiplication table shall I generate for you? ")
print "\nHere is your", tablenum, "times table:\n"

# below is the long, and wrong way to solve this problem!
#print "1 times", tablenum, "is", tablenum
#print "2 times", tablenum, "is", tablenum*2
#print "3 times", tablenum, "is", tablenum*3
#print "4 times", tablenum, "is", tablenum*4

# below is the right, and short way to solve it with a loop
for i in range(1, 13):
    print i, "times", tablenum, "is", i*tablenum
    print "------------------"
print"\nHope you found that useful!"
 
